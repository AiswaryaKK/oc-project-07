import csv
from itertools import combinations
import time

DATA_PATH_CSV = "./data/dataset_bruteforce.csv"
MAX_EXPENSE = 500


def data_to_list(csvfilepath: str):
    """Read csv file and create a list with the data.
    Arg:
    csvfilepath (str) - path to the csv file containing data

    Return:
    shares_list (list)"""
    with open(csvfilepath) as csvfile:
        shares_file = csv.reader(csvfile)
        """ Skip header """
        next(shares_file)
        """ Create list of shares """
        shares_list = [(row[0], float(row[1]), float(row[2])) for row in shares_file]
        return shares_list


def calculate_price(combination: tuple):
    """Calculate total price.
    Arg:
    combination (tuple) - tuple of shares

    Return:
    total price (float)"""
    total_price = 0
    for share in combination:
        total_price += share[1]
    return total_price


def calculate_profit(combination: tuple):
    """Calculate total profit.
    Arg:
    combination (tuple) - tuple of shares

    Return:
    total profit (float)"""
    total_profit = 0
    for share in combination:
        profit = (share[1] * share[2]) / 100
        total_profit += profit
    return total_profit


def get_possible_combos(shares_list: list):
    """Get all the possible combination of shares with expense less than the MAX_EXPENSE.
    Args:
    shares_list (list) - list of all the shares

    Return:
    possible_combos (list) - list of possible combination"""
    possible_combos = []
    for i in range(len(shares_list) + 1):
        all_combos = combinations(shares_list, i)

        for element in all_combos:
            total_price = calculate_price(element)

            if total_price <= MAX_EXPENSE:
                possible_combos.append(element)

    return possible_combos


def get_best_combos(possible_combinations: list):
    """Get most profitable combination of shares.
    Arg:
    possible_combinations (list) - list of shares

    Return:
    best_combos (list)"""
    profit = 0
    best_combos = []
    for element in possible_combinations:
        total_profit = calculate_profit(element)

        if total_profit > profit:
            profit = total_profit
            best_combos = list(element)

    return best_combos


def display_results(best_combination: list, total_profit: float, total_price: float):
    print("\nMost profitable combination of shares:\n")
    for share in best_combination:
        print(f"{share[0]}   |  {share[1]}€   |  {share[2]}%")
        print("------------------------------------------")

    print("\nTotal expense: ", total_price)
    print("Profit in euro: ", total_profit)


def main():
    start_time = time.time()
    shares_list = data_to_list(DATA_PATH_CSV)
    possible_combinations = get_possible_combos(shares_list)
    best_combination = get_best_combos(possible_combinations)
    total_price = calculate_price(best_combination)
    total_profit = calculate_profit(best_combination)
    end_time = time.time()
    display_results(best_combination, total_profit, total_price)
    print("Time for execution: ", end_time - start_time, "seconds")


if __name__ == "__main__":
    main()
