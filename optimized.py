import sys
import csv
import time


""" Check for custom data file name (default = ./data/dataset1_Python+P7.csv) """
try:
    DATA_PATH_CSV = "./data/" + sys.argv[1] + ".csv"
except IndexError:
    print("\nFile not found. Using the default file data/dataset1_Python+P7.csv.\n")
    DATA_PATH_CSV = "./data/dataset1_Python+P7.csv"

""" Check for custom maximum expense value (default = 500) """
try:
    MAX_EXPENSE = float(sys.argv[2])
except IndexError:
    MAX_EXPENSE = 500


def data_to_list(csvfilepath: str):
    """Read csv file and create a list with the data.
    Arg:
    csvfilepath (str) - path to the csv file containing data

    Return:
    shares_list (list)"""
    with open(csvfilepath) as csvfile:
        shares_file = csv.reader(csvfile)
        """ Skip header """
        next(shares_file)
        """ Create list of shares """
        shares_list = [(row[0], float(row[1]), float(row[2])) for row in shares_file]
        return shares_list


def display_results(best_combination: list, total_profit: float, total_price: float):
    print("\nMost profitable combination of shares:\n")
    for share in best_combination:
        print(f"{share[0]}   |  {share[1]}€   |  {share[2]}%")
        print("------------------------------------------")

    print("\nTotal expense: ", total_price)
    print("Profit in euro: ", total_profit)


def main():
    start_time = time.time()

    shares_list = data_to_list(DATA_PATH_CSV)
    """ Sort the list in the descending order of profit """
    sorted_list = sorted(shares_list, key=lambda share: share[2], reverse=True)
    """ Make the list of most profitable combination by going through the first
    part of the sorted list. If the total cost is greater than the limit break the loop. """
    total_cost = 0
    total_profit = 0
    most_profitable_combination = []
    for share in sorted_list:
        price = share[1]
        if price > 0 and (total_cost + price) <= MAX_EXPENSE:
            total_cost += price
            total_profit += (share[1] * share[2]) / 100
            most_profitable_combination.append(share)
        elif total_cost > MAX_EXPENSE:
            break

    end_time = time.time()

    display_results(most_profitable_combination, total_profit, total_cost)
    print("Time for execution: ", end_time - start_time, "seconds")


if __name__ == "__main__":
    main()
