# Best combination of actions with different algorithms
This project is part of my degree program at Openclassrooms (project 07: Résolvez des problèmes en utilisant des algorithmes en Python).

It includes two approaches to calculating the best combination of actions: the first using the brute force algorithm, and the second using an optimized way.

This project is done using Python 3.9.7.

## Run the scripts

### Brute force
    python bruteforce.py

The limit on the amount of investment is 500 euros.This script works with the dataset_bruteforce.csv file contained in the data/ directory.

### Optimized
    python optimized.py

The limit on the amount of investment is 500 euros, and the data treated is data/dataset1_Python+P7.csv by default. 
One can change the file by giving the file name without the extension. Like in the following example:

    python optimized.py dataset2_Python+P7

To change the limit on the amount of investment:

    python optimized.py dataset2_Python+P7 600

