import math
import matplotlib.pyplot as plt

MAXIMUM_NUMBER_OF_ACTIONS = 100

number_of_elements = []
time_complexity_of_elements = []
for k in range(1, MAXIMUM_NUMBER_OF_ACTIONS):
    time_complexity = k * math.log(k)
    number_of_elements.append(k)
    time_complexity_of_elements.append(time_complexity)

# Time complexity = nlog(n)
x = number_of_elements
y = time_complexity_of_elements

plt.plot(x, y, color="b")
plt.title(
    "Big-O de l'algorithme optimisé pour complexité temporelle = Logarithmique Linéaire"
)
plt.show()

# Space complexity = n
x = number_of_elements
y = number_of_elements

plt.plot(x, y, color="b")
plt.title("Big-O de l'algorithme optimisé pour complexité spatiale = Linéaire")
plt.show()
