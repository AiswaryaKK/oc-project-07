import math
import matplotlib.pyplot as plt

NUMBER_OF_ACTIONS = 20

number_of_combinations = []
number_of_elements = []
total_number_of_elements = 0
list_of_number_of_elements = []
for k in range(NUMBER_OF_ACTIONS):
    result = math.factorial(NUMBER_OF_ACTIONS) / (
        math.factorial(k) * (math.factorial(NUMBER_OF_ACTIONS - k))
    )
    total_number_of_elements += result
    list_of_number_of_elements.append(total_number_of_elements)
    number_of_elements.append(k)
    number_of_combinations.append(result)

# plot number of combinations generated each time in loop
x = number_of_elements
y = number_of_combinations

plt.xlabel("Nombre d'élément")
plt.ylabel("Nombre de combinaison possible")
plt.plot(x, y, color="b")
plt.title("Graphique de combinaisons possibles")
plt.show()

# plot total number of combinations for given number of elements
x_02 = number_of_elements
y_02 = list_of_number_of_elements

plt.xlabel("Nombre d'action")
plt.ylabel("Nombre de combinaison possibles")
plt.plot(x_02, y_02, color="b")
plt.title("Graphique de combinaisons possibles pour nombre d'actions")
plt.show()
